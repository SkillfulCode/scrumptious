<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/WayneBasile/scrumptious-recipes/-/blob/main/README.md?ref_type=heads">
    <img src="logo.svg" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Scrumptious Recipes</h3>

  <p align="center">
    Welcome to Scrumptious Recipes! Our platform makes cooking easier for everyone with a seamless way to sign in, create, and discover delicious recipes. Say goodbye to endless searching for the perfect recipe!
    <br />
    <br />
    <a href="https://gitlab.com/WayneBasile/scrumptious-recipes/-/blob/main/README.md?ref_type=heads"><strong>Explore the Docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/WayneBasile/scrumptious-recipes/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/WayneBasile/scrumptious-recipes/-/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#local-installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Scrumptious Recipes is a full-stack Django application with a user-friendly interface for creating, managing, and sharing recipes and their ingredients.

### Built With

[![Django][Django.com]][Django-url]

[![Python][Python.org]][Python-url]

[![HTML5][HTML5.com]][HTML5-url]

[![CSS3][CSS3.com]][CSS3-url]

[![SQLite][SQLite.org]][SQLite-url]

<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

The installation instructions assume your system has the following software: [Google Chrome](https://www.google.com/chrome/), [PIP](https://pypi.org/project/pip/), and [pyvenv](https://github.com/pyenv/pyenv).

If you don't have these (or equivalent) software, please install them before proceeding.

To get a local copy of Scrumptious Recipes up and running on your machine follow these simple steps.

### Local Installation

1. Fork and clone the [repository](https://gitlab.com/WayneBasile/scrumptious-recipes/-/forks/new)

2. In terminal, run `cd <repository_directory>`

3. In terminal, run `python -m venv ./.venv`

4. In terminal, run `source ./.venv/bin/activate`

5. In terminal, run `pip install --upgrade pip`

6. In terminal, run `pip install -r requirements.txt`

8. In terminal, run `python manage.py migrate`

9. In terminal, run `python manage.py runserver`

10. Navigate to [localhost:8000](http://localhost:8000/)



<!-- CONTACT -->
## Contact

[![Contributors][wayne-shield]][wayne-url]



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

[Django](https://www.djangoproject.com/) · [Python](https://www.python.org/) · [SQLite](https://www.sqlite.org/index.html)

[HTML5](https://developer.mozilla.org/en-US/docs/Web/HTML) · [CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS)

[Shields.io](https://shields.io/) · [Simple Icons](https://simpleicons.org/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[Django.com]: https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=Django&logoColor=white
[Django-url]: https://www.djangoproject.com/

[Python.org]: https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python-url]: https://www.python.org/

[SQLite.org]: https://img.shields.io/badge/SQLite-003B57?style=for-the-badge&logo=sqlite&logoColor=white
[SQLite-url]: https://www.sqlite.org/index.html

[HTML5.com]: https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white
[HTML5-url]: https://developer.mozilla.org/en-US/docs/Web/HTML

[CSS3.com]: https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white
[CSS3-url]: https://developer.mozilla.org/en-US/docs/Web/CSS

[wayne-shield]: https://img.shields.io/badge/Wayne_Basile-0A66C2?logo=linkedin&style=for-the-badge
[wayne-url]: https://www.linkedin.com/in/waynebasile/
