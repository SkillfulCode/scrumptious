# this (project) urls.py is where we register our other (apps) urls.py
from django.contrib import admin
# we add the import statement because we want to inclue stuff from the other url file
# that's why we import the include path as well
from django.urls import path, include
# this is required to redirect to the home page
from django.shortcuts import redirect

# placed before urlpatterns because its used in the path registration
def redirect_to_recipe_list(request):
    # the name matches the view
    return redirect("recipe_list")

urlpatterns = [
    path('admin/', admin.site.urls),
    # this function is to inclue the urls we wrote in the recipes app urls.py
    # path('', include('recipes.urls')),
    # this was changed so that recipes can be excluded in the app's url patterns
    # it's usually best practice to use the app's name as the prefix
    path('recipes/', include('recipes.urls')),
    path("", redirect_to_recipe_list, name="home_page"),
    path("accounts/", include("accounts.urls")),
]
