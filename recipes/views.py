# redirect is imported for the create view
from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
# imported for the create view
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


# this is a function that allows django to respond to HTTP requests
# if id isn't aded, there will be a TypeError because django is calling our function with an extra parameter named id
def show_recipe(request, id):
    # because we're getting the id from the URL path, and using it to get data from the database we used the get_object_or_404 shortcut so that django will create a 404 error when the recipe deson't exist for the provided id
    recipe = get_object_or_404(Recipe, id=id)
    # context (which usually a dictionary) gets the Recipe object into the HTML template so that we can use the data from the database
    context = {
        # because the key in the dictionary is "recipe_object, that will be the name of the variable that we'll use in the template to get to the recipe object
        "recipe_object": recipe,
    }
    # the render function alwasy takes the request object as its first argument
    # the second argument is the path to our html file that django will send back to the browser
    # that path, recipes/detail.html, will need to be inside the templates directory
    # we pass the context dictionary as the third arugment of the render function
    return render(request, "recipes/detail.html", context)


# because we're getting all of the Recipe objects, we don't need one id to specify a single recipe
def recipe_list(request):
    # the all method gets all the recipes
    recipes = Recipe.objects.all()
    # makes the list of recipes data avilable using the {{ }} syntax when passed into render function
    context = {
        "recipe_list": recipes,
    }
    # chose which HTML file to use and render it
    return render(request, "recipes/list.html", context)


# only logged in users can create a recipes
@login_required
def create_recipe(request):
    if request.method == "POST":
        # POST is when a form is submitted
        form = RecipeForm(request.POST)
        # the form is used to validate the values and save to the database
        if form.is_valid():
            # have the form, save the recipe, but not to the database
            recipe = form.save(False)
            # assign the current user to the author property of the recipe which can be accessed in request.user
            recipe.author = request.user
            # save the recipe using the recipe's save method, not the form
            recipe.save()
            # if the process completes, redirect to a different page
            return redirect("recipe_list")

    else:
        # creates an instance of the Django model form class
        form = RecipeForm()

    # puts the form in the context
    context = {
        "form": form,
    }
    # rendes the HTML template with the form
    return render(request, "recipes/create.html", context)


@login_required
# get the object to edit
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)

    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            # how to redirect to the edited recipe
            return redirect("show_recipe", id=id)

    else:
        # create a form with the objects data and show it in the HTML
        form = RecipeForm(instance=recipe)

    context = {
        "form": form,
        "recipe_object": recipe,
    }
    return render(request, "recipes/edit.html", context)


@login_required
# filtering to show only recipes the user has created
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
