from django.contrib import admin
# in order to create our class, we need to import the Recipe model
from recipes.models import Recipe, RecipeStep, Ingredient


# This is the decorator used to to register the Recipeadmin model admin with the Recipe model class
@admin.register(Recipe)
# this class inherits from the ModelAdmin class and is an admin for our models, such as our Recipe model class
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = (
        "amount",
        "food_item",
        "id",
    )
