# imports the path function from django.urls
from django.urls import path
# imports the functions we wrote in views.py to tell django it exists
from recipes.views import show_recipe, recipe_list, create_recipe, edit_recipe, my_recipe_list

# django expecs all path-function registrations to be in a list with the variable urlpatterns
urlpatterns = [
    # this path function associates the url path "recipes/" with the show_recipe function
    # added id to path, which will look for an integer value after the recipes/ part of the URL
    # the name is the path registration used to generate URLs in the HTML template
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("", recipe_list, name="recipe_list"),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/edit/", edit_recipe, name="edit_recipe"),
    # the "recipes/" was required before updating the projects URL's path with recipes/
    # path("recipes/<int:id>/edit/", edit_recipe, name="edit_recipe"),
    path("mine/", my_recipe_list, name="my_recipe_list"),
]
