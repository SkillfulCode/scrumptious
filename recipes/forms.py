from django.forms import ModelForm
from recipes.models import Recipe

# define a class that inherits from django.forms.ModelForm class
# because the create and edit views allow users to provide the same data for recipes, we'll be able to reuse the form class for both view functions and screens
class RecipeForm(ModelForm):
    # create an inner class (a class inside a class) named Meta
    class Meta:
        # specify which Django model it should work with
        model = Recipe
        # specify the fields we want to show
        fields = [
            "title",
            "picture",
            "description",
        ]
